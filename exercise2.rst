Exercise 2
==========

Inspect a modulefile
--------------------

There are several commands to help you inspect a modulefile including ``module help``, ``module show``, ``module whatis`` which we will discuss in this section

Get help section for a module::

   module help anaconda2
   ml help anaconda2

Get whatis section of module::

   module whatis anaconda2
   ml whatis anaconda2

To show commands run in modulefile you can run::

   module show anaconda2

The `module show` will not report content of modulefile, if you want to view the file content you can 
use the ``--raw`` option::

   ml --raw show  anaconda2
   module --raw show anaconda2 

Please note that ``--raw`` is an option to **module** and **ml** and must be specified after ``ml`` and ``module`` command and not after the sub-commands. If you 
specify **module show --raw gcc** that will result in an error.

By default, module commands are redirected to stderr as pose to stdout this means if you run the following, it won't
grep the output as you expect::

        ml --raw show anaconda2 | grep family 

To circumvent this issue, the ``--redirect`` option must be used to redirect output of **stderr** to **stdout** stream. By default **module [list|avail|spider]** command will send output to stderr therefore command chaining won't work as you expect.

In this example we can use the ``--redirect`` option to show how we can use **grep** utility to search file some string in the modulefile.

::

        $ ml --redirect --raw show anaconda2 | grep family
        family("Python")

Alternately, you can set ``LMOD_REDIRECT=True`` if you want to enable this feature by default. For more details see `Lmod FAQ <https://lmod.readthedocs.io/en/latest/040_FAQ.html>`_

If you want to view machine readable output of `module av`, `module list` or `module spider` you can do the following::

        ml -t
        ml -t av
        ml -t spider


We have set module families at Compiler and MPI level see what happens when you load ``gcc`` followed by ``pgi``. You will notice
Lmod will autoswap gcc with pgi because we set ``family("compiler")`` in the modulefiles::

   ml purge
   ml gcc
   ml pgi

If you did this correctly you should see the following message::

        
        $ ml pgi

        Lmod is automatically replacing "gcc/5.0" with "pgi/18.10".

This is because Lmod detects `gcc` and `pgi` belong to same family group and `gcc` was loaded by default and when loading `pgi` it automatically swaps the module. 

To see contents of `gcc` and `pgi` module you can run the following::

        ml --raw show gcc pgi

Lmod sets environment variables ``LMOD_FAMILY_<family-name>`` when loading family modules. Let's see 
the environment variables set by running the following::

   env | grep LMOD_FAMILY

By default **LMOD_AUTO_SWAP** is enabled which  allows Lmod to swap any modules that use the family function such as compilers and mpi stacks. This can be 
disabled if you don't want autoswaping Lmod families or don't care about Lmod families all together. You can check your module configuration
using ``module --config`` and see the following entry::

        $ module --config 2>&1 | grep "Auto swapping"
        Auto swapping                      yes

Lmod autoswap modules of same name if you choose different versions, to demonstrate we will load two versions of gcc::

        ml purge
        ml gcc
        ml 
        ml gcc/6.0
        ml 

This is configured using environment **LMOD_DISABLE_NAME_AUTOSWAP** which is set to **no** which indicates two instances of 
same module can't be loaded together. You can check the configuration value by running::

        $ module --redirect --config 2>&1 | grep "Disable Same Name AutoSwap"
        Disable Same Name AutoSwap         no

If you want to load two versions of same software you can set ``LMOD_DISABLE_NAME_AUTOSWAP="yes"`` as environment to override in your user shell
or set it persistent across all users in systemwide Lmod configuration. It's generally advised to not enable this setting because users shouldn't 
be loading two versions of same library at same time which will lead to unexpected behavior, however if they must they can override this setting in their
user shell.

Let's talk about sticky modules, as you may note the ``gcc/6.0`` is a sticky module which can't be removed using **module purge** or 
**module unload**, you must run ``module --force purge`` to remove all modules including sticky modules. Sticky modules are useful
on startup modules (i.e `LMOD_SYSTEM_DEFAULT_MODULEFILES`) so that **module purge** doesn't remove startup modules. If your startup modules
set `MODULEPATH`, and you remove all your startup modules you may end up with an empty `MODULETREE` and then you won't be able to load any modules.



Hidden Modules
--------------

Hidden modules in Lmod are precedded by a ``.`` followed by the name of module. Lmod
treats these modules hidden when you run ``module av``. This can be useful if you want to hide software packages such as dependencies for particular software.

To view hidden modules use the ``--show_hidden`` flag::

        module --show_hidden av 
        ml --show_hidden av

To view hidden modules with ``module spider`` you must set ``--show_hidden``::
       
        ml --show_hidden spider bzip2


If you want to view Lmod configuration you can run:: 

   module --config

Pay close attention to Lmod Properties Table (**propT**) as we will cover this in later exercise.

If you want a terse output from module subcommands ``avail``, ``spider``, ``list`` or ``savelist``  you 
can use ``-t`` flag for terse output::

   ml -t
   ml -t av 
   ml -t spider 
   ml -t savelist

If you want to search for text in the modulefile try ``module key``::

   module key gcc

Debugging Modules
-------------------

Let's talk about few ways to debug modules. 

Lmod can trace modules using ``module -T`` this can be useful when understanding how modulefiles are interacted with each other.
In this example we load ``openmpi`` with trace mode::

        bash-4.2$ module -T load openmpi
        running: module -T load openmpi
          Loading: openmpi (fn: /global/cscratch1/sd/siddiq90/lmod-training/modules/ex2/openmpi/2.1.0-gcc-6.x.lua)
            Loading: gcc (fn: /global/cscratch1/sd/siddiq90/lmod-training/modules/ex2/gcc/6.0.lua)

The module table feature ``module --mt`` is quite useful when troubleshooting current state of modulefiles in your environment. In this example we load
the following modules and run ``module --mt``::

        ml --force purge
        ml anaconda2 Go gcc
        module --mt

!!! note 
    We need to run `ml --force purge` because `gcc/6.0` is a sticky module


The ``--mt`` will show current state of modules, families, module properties, and list of module trees::
     
  bash-4.2$ module --mt
  _ModuleTable_ = {
    ["MTversion"] = 3,
    ["c_rebuildTime"] = false,
    ["c_shortTime"] = false,
    depthT = {},
    family = {
      ["Python"] = "anaconda2",
      ["compiler"] = "gcc",
    },
    mT = {
      Go = {
        ["fn"] = "/global/cscratch1/sd/siddiq90/lmod-training/modules/ex2/Go/1.9.lua",
        ["fullName"] = "Go/1.9",
        ["loadOrder"] = 2,
        propT = {},
        ["stackDepth"] = 0,
        ["status"] = "active",
        ["userName"] = "Go",
      },
      anaconda2 = {
        ["fn"] = "/global/cscratch1/sd/siddiq90/lmod-training/modules/ex2/anaconda2/4.3.0.lua",
        ["fullName"] = "anaconda2/4.3.0",
        ["loadOrder"] = 1,
        propT = {},
        ["stackDepth"] = 0,
        ["status"] = "active",
        ["userName"] = "anaconda2",
      },
      gcc = {
        ["fn"] = "/global/cscratch1/sd/siddiq90/lmod-training/modules/ex2/gcc/5.0.lua",
        ["fullName"] = "gcc/5.0",
        ["loadOrder"] = 3,
        propT = {},
        ["stackDepth"] = 0,
        ["status"] = "active",
        ["userName"] = "gcc",
      },
    },
    mpathA = {
      "/global/cscratch1/sd/siddiq90/lmod-training/modules/ex1", "/global/cscratch1/sd/siddiq90/lmod-training/modules/ex2", "/opt/apps/software/Lmod/modulefiles/Linux"
      , "/opt/apps/software/Lmod/modulefiles/Core", "/opt/apps/software/Lmod/lmod/lmod/modulefiles/Core",
    },
    ["systemBaseMPATH"] = "/global/cscratch1/sd/siddiq90/lmod-training/modules/ex1:/global/cscratch1/sd/siddiq90/lmod-training/modules/ex2:/opt/apps/software/Lmod/modulefiles/Linux:/opt/apps/software/Lmod/modulefiles/Core:/opt/apps/software/Lmod/lmod/lmod/modulefiles/Core",
  }
 

Module collections
------------------

Lmod introduce the concept of `module collections <https://lmod.readthedocs.io/en/latest/010_user.html#user-collections>`_ which allows user to reference a group of modules with a collection name. This 
is particularly useful if you need to load several modules and don't recall all the module names, you can save the modules into a collection. Lmod can only load one module collection at a time using ``module restore`` command. We
will discuss how to save module collection, show content of collection, restore from collection and list all collections.


Let's load a few modules and save them using ``ml save``::

        ml purge
        ml gcc/6.0 openmpi/2.1.0-gcc-6.x
        ml save


Your default collection will be named ``default`` and all module collections are
stored in **$HOME/.lmod.d/**

You can add multiple collections using ``module save <collection-name>``. Let's add
one more collection as follows::

        # --force purge because gcc/6.0 is sticky module
        ml --force purge
        ml pgi
        ml save pgi-default


To view all your collections you can run ``ml savelist``::

        ml savelist

To view modules in a collection you can run ``ml describe`` or ``module describe`` and it will
show the `default` collection::

        ml describe

This is equivalent to running ``ml describe default``   

Next, let's see content of collection ``pgi-default``::
    
     bash-4.2$ ml describe pgi-default
        Collection "pgi-default" contains: 
        1) pgi

To restore a collection in your user shell use ``ml restore <collection-name>``::

        bash-4.2$ ml purge
        bash-4.2$ ml restore pgi-default
        Restoring modules from user's pgi-default
        bash-4.2$ ml

        Currently Loaded Modules:
        1) pgi/18.10 (g)

        Where:
        g:  built for GPU

To restore the ``default`` collection run::

        module restore

To learn more about module collections see https://lmod.readthedocs.io/en/latest/010_user.html#user-collections-label 


Customize MODULEPATH
----------------------

If you want to add new module trees into your MODULEPATH you can do that
by using ``module use`` or set ``MODULEPATH`` variable::

   module use <path>


By default ``module use`` will prepend the path into variable MODULEPATH as you may 
notice if you run:: 

   echo $MODULEPATH

module will search each module tree in the order specified by MODULEPATH, there 
may be an instance where two modules have exact same module version (i.e ``gcc/5.0`` found in 2 trees),
in that case Lmod will load the module from the 1st module tree in MODULEPATH value

You can also append module tree to MODULEPATH using ``-a`` option::

   module use -a <path>
   
Let's unset MODULEPATH and add Lmod tree to MODULEPATH as follows:: 
  
   unset MODULEPATH
   module use $LMOD_PKG/modulefiles/Core
   echo $MODULEPATH

Now if we list all modules we should see the following::

        bash-4.2$ module -t av
        /opt/apps/software/Lmod/lmod/lmod/modulefiles/Core:
        lmod
        settarg

MODULEPATH stores a set of module trees that is used by ``module`` command to view
all module trees. 


What is module
--------------

Is module a Linux command?

No its a shell wrapper defined as follows::

        module is a function
        module () 
        { 
        eval $($LMOD_CMD bash "$@") && eval $(${LMOD_SETTARG_CMD:-:} -s sh)
        }


Lmod Spider 
------------

The `spider <https://lmod.readthedocs.io/en/latest/136_spider.html>`_ command is used for building spider cache which is often
used by administrators when configuring Lmod to show the latest software stack. Note that ``spider`` is different than ``module spider``, 
the later is used by end-users for finding modules.


First load the ``lmod`` and you should see the spider help menu for list of options::

        ml lmod
        spider --help


To show a list of all modules use the ``-o list`` option which shows full path to all modulefiles::

        spider -o list $MODULEPATH


Lmod ``spider`` can be useful for generating `software pages <https://lmod.readthedocs.io/en/latest/136_spider.html#software-page-generation>`_ for documentation purposes. 
For example we can use the ``spider -o jsonSoftwarePage`` to  show content of all modules in json content and parse this using python **json.tool** package::

        spider -o jsonSoftwarePage $MODULEPATH | python -m json.tool


The content below shows the ``gcc`` entry and all available versions including default version::

        {
                "defaultVersionName": "5.0",
                "package": "gcc",
                "versions": [
                {
                        "canonicalVersionString": "000000006.*zfinal",
                        "full": "gcc/6.0",
                        "help": "GCC help",
                        "markedDefault": false,
                        "path": "/global/cscratch1/sd/siddiq90/lmod-training/modules/ex2/gcc/6.0.lua",
                        "properties": {
                        "lmod": {
                                "sticky": 1
                        }
                        },
                        "versionName": "6.0",
                        "wV": "000000006.*zfinal"
                },
                {
                        "canonicalVersionString": "000000005.*zfinal",
                        "full": "gcc/5.0",
                        "help": "\"GCC help",
                        "markedDefault": true,
                        "path": "/global/cscratch1/sd/siddiq90/lmod-training/modules/ex2/gcc/5.0.lua",
                        "versionName": "5.0",
                        "wV": "^00000005.*zfinal"
                }
                ]
        },



Another useful command to show json structure using output format ``-o spider-json`` which shows metadata for each module entry in spider cache::

        spider -o spider-json $MODULEPATH | python  -mjson.tool


To learn more about how to update spider cache see `spider cache <https://lmod.readthedocs.io/en/latest/130_spider_cache.html>`_
